#### The simpliest way to calculate availability (A) for a device, system with N inline elements or N parallel.

##### Terms:
**MTBF** = Mean time between failures.

A predicted average amount of time to a fail.
Counted in hours and provided by vendor.

Examples:
- MTBF Juniper EX4600 is 150,000 hours [Datasheet](http://www.juniper.net/assets/us/en/local/pdf/datasheets/1000511-en.pdf)
- MTBF Cisco Catalyst 3850 (WS-C3850-24XU) is 203,150 hours [Datasheet](http://www.cisco.com/c/en/us/products/collateral/switches/catalyst-3850-series-switches/data_sheet_c78-720918.html)

MTBF for links is:
- provided by SP (0.995 an average)
- counted as three nines (0.999) for our indoor links
- counted same as SP links (0.995) for our outdoor links

**MTTR** = Mean time to repair

A predicted average amount of time to replace or restore a failed item.
Counted in hours and provided by:
- vendor in case of a service with a replacement option
- operating team in charge of monitoring and replacement

Examples:
- 72 hours to fix an ACL being rejected all traffic on a router 2000 kilometers away
- 5 hours to replace a device's single PSU (4 hours replacement vendor's service contract + 1 hour to replace)
- 3 hours SLA with SP
- 1 hour to replace an optical patchcord between floors

##### Equalties for calculations
Single item `A = MTBF / (MTBF + MTTR)`

System with inline items `A = A1*An`

System with parallel items `A = 1-((1-A1)*(1-An))`

### TODO
- ~~rewrite inline() to use element()~~
- ~~add the function to a parallel calculation~~
- ~~provide actual docstrings~~
- refactor a code to increase readability + PEP8 compliance
- class-based?

### Usage
Populate 'catalogue' list with actual data

Use appropriate functions to calculate availability

### Examples
---
Calculation #1: a single router with a single SP uplink

```
  L1  ____
-----| D1 |
     |____|
```

Execute: `print('1 router, 1 SP uplink:', inline('D1',link_sp)*100)`

Output: `1 router, 1 SP uplink: 99.4641928905594` (two nines)

---

Calculation #2: a single router with two SP uplinks

```
  L1  ___  L2
-----| D1 |-----
     |____|
```

Execute: `print('1 router, 2 uplinks:', parallel(link_sp,link_sp)*single('D1')*100)`

Output: `1 router, 2 uplinks: 99.96151385501219` (three nines)

---

Calculation #3: two routers each with a single uplink

```
  L1  ____
-----| D1 |
     |____|
  L2  __|_
-----| D2 |
     |____|
```

Execute: `print('2 router, 2 uplinks:', parallel(link_sp,link_sp)*parallel('D1','D2')*100)`

Output: `2 router, 2 uplinks: 99.9974942431379` (four nines)

---

Calculation #4: two routers each with a single uplink and shared LAN

```
  L1  ____
-----| D1 |--|
     |____|  |
  L2  __|_   |---
-----| D2 |--|
     |____|
```

Execute: `print('2 router, 2 uplinks, shared LAN:', parallel(link_sp,link_sp)*parallel('D1','D2')*single(link_indoor)*100)`

Output: `2 router, 2 uplinks, shared LAN: 99.98749449371358` (three nines)
