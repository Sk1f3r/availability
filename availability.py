#
### Calculate simple availability for a device, system with N inline elements or N parallel.
#
# MTBF = Mean time between failures.
# MTTR = Mean time to repair
###
# Constants
link_sp = 0.995
link_indoor = 0.9999
link_outdoor = 0.995
#
# Devices and links
# Further, Item means a device or a link
catalogue = [
    {'name':'D1', 'MTBF':200000, 'MTTR':72},
    {'name':'D2', 'MTBF':150000, 'MTTR':24},
    {'name':'D3', 'MTBF':100000, 'MTTR':4},
    {'name':'L1', 'MTBF':500000, 'MTTR':4, 'desc':'SP'}
]

# Functions
def single(item, out=0):
    '''
    A function to calculate an availability for a single item.
    Being used by any other functions with human-readable output format.
    Equalty: A = MTBF / (MTBF + MTTR)
    :param item: name of an item as string, an example 'D1"
    :param out: if formatted output is required set 1
    :return: float or string, depends on the "out" variable
    '''
    if type(item) is float:
        return item # return as is if a float type constanta like link_sp
    else:
        for obj in catalogue: # scan the catalogue to find the item
            if obj['name'] == item:
                dev = catalogue.index(obj) # get the item dict by index of a list
                A = catalogue[dev]['MTBF'] / (catalogue[dev]['MTBF'] + catalogue[dev]['MTTR']) # calculate
                if out:
                    return '{} availability is {}'.format(item, float(round(A*100,5))) # formatted
                else:
                    return A # float
    return 'Device not found'
#
#
def inline(*args, out=0):
    '''
    A function to calculate an availability for a subset of items.
    Each pair of these items are interconnected directly.
    Equalty: A = A1 * An
    Always less than the lowest A of all items.
    :param args: a list of items
    :param out: if formatted output is required set 1
    :return: float or string, depends on the "out" variable
    '''
    A = 1
    for arg in args:
        sub_A = single(arg) # calculate A for every item in a system
        A *= sub_A # multiply overall availability by A of every item
    if out:
        return 'Availability for inline devices equals: {}'.format(round(A * 100,5)) # formatted
    else:
        return A # float
#
#
def parallel(*args, out=0):
    '''
    A function to calculate an availability for a subset of items.
    The subset is a redundant group, which means a fail of any single item
    is not a fail of a whole system.
    Equalty: A = 1-(1-A1)(1-An)
    Always higher than the highest A of all items.
    :param args: a list of items
    :param out: if formatted output is required set 1
    :return: float or string, depends on the "out" variable
    '''
    A = 1
    for arg in args:
        sub_A = single(arg) # calculate A for every item in a system
        A *= (1-sub_A) # multiply pre-overall availability by A of every item
    A = 1.0 - A
    if out:
        return 'Availability for parallel devices equals: {}'.format(round(A*100,7)) # formatted
    else:
        return A # float
#
### Examples
print(single('D1'))
print(inline('D1',link_sp,'D2'))
print(parallel('D1','D3'))
print('1 router, 1 SP uplink:', inline('D1',link_sp)*100)
print('1 router, 2 uplinks:', parallel(link_sp,link_sp)*single('D1')*100)
print('2 router, 2 uplinks:', parallel(link_sp,link_sp)*parallel('D1','D2')*100)
print('2 router, 2 uplinks, shared LAN:', parallel(link_sp,link_sp)*parallel('D1','D2')*single(link_indoor)*100)

# EOF